# ----------- Validación de una entrada --------- #
# Pedir que un usuario ingrese un número entre 0 y 36, volver a pedir hasta que se cumpla la condición
# || = or
# && = and
puts 'Ingresa un número entre 0 y 36:'
numero = -1
numero = gets.chomp.to_i while numero < 0 || numero > 36

# ------------------- TIMES ------------------ #
5.times { puts "Bart está escribiendo" }
5.times { |i| puts "#{i + 1} vez..." }

10.times do |i|
  if i == 0 
    puts "#{i + 1} elefante ..." # interpolación #{}
  else
    puts "#{i + 1} elefantes ..." # interpolación #{}
  end
end

# ----------------- FOR y EACH ----------------------- #
for i in 0..5
  puts i
end

(0..5).each do |i|
  puts i
end