# ----------- VALIDACIÓN DE PORCENTAJE -------------- #

# Crear un método para validar un porcentaje
# Para eso, el método recibe el porcentaje como argumento. 
# Si el valor está entre 0 y 100, debe devolver 'true' 
# y si está fuera de rango, debe devolver 'false'

def validador(porcentaje)
  if porcentaje >= 0 && porcentaje <= 100
    true
  else
    false
  end
end

puts validador(50)
puts validador(100)
puts validador(130)
puts validador(-150)