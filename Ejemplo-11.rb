# ---------------- ITERACIONES -------------------- #
# Iterando un arreglo:
array_1 = [1,2,6,5,8,9,45]

# Ejemplo 1:
for element in array_1 do
  print "#{element}, "
end

puts
# Ejemplo 2:
array_1.each do |i|
  print "#{i}, "
end

puts
# Ejemplo 3:
array_1.each { |i| print "#{i}, " }
puts

# Ejemplo 4:
# Crear un método para saber si un elemento se encuentra dentro de un arreglo
# Nuestro método debe recibir un arreglo y un string.
# Si el string se encuentra dentro de un arreglo, debe devolver 'true', en caso contrario 'false'.
array_2 = [1, 2, 4, 5, 'perro', 'gato', 'guitarra', 10, 24]

def check_if_exist?(array, match)
  array.each do |i|
    return true if i == match # Si el elemento existe dentro del array, devuelve 'true'
  end

  false # en caso contrario, devuelve 'false'
end

puts check_if_exist?(array_2, 4)
puts check_if_exist?(array_2, 45)

# Mejor forma de resolver el ejercicio con el método 'include?()'
puts array_2.include?(4)
puts array_2.include?(45)

