# -------- ITERANDO UN ARREGLO CON INDICE -------- #
array_1 = ['mariana', 'javier', 'moly', 12, 3, 5, 6]

array_1.each_with_index do |value, index|
  puts "#{index} => #{value}"
end

# Ejercicio 1:
# Iterar el siguiente 'array'. Cada vez que el elemento sea divisible por 3, imprimir "foo" junto al elemento y si el índice es divisible por 4, entonces imprimir "bar" junto al índice
array_2 = [1,2,3,4,5,6,7,8,9,10,11,12]

array_2.each_with_index do |element, index|
  if (element % 3).zero?
    puts "foo #{element}"
  end

  if (index % 4).zero?
    puts "bar #{index}"
  end
end
