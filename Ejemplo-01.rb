# ----------- Identificadores en Ruby --------- #
a = 5
puts a

b = 6
puts b

c = a + b
puts c

d = 'hola mundo'
puts d

a1 = 5
a1 += 2
puts a1
a1 -= 2
puts a1

# ----------- Literales y captura de datos --------- #
# Con 'gets' podemos capturar datos desde el teclado
# con 'puts' y 'print' podemos mostrar el resultado
# 'chomp' elimina el salto de línea introducido al presionar 'enter'

puts "Cuál es tu nombre?" # Hacemos una pregunta
b2 = gets.chomp # capturamos el dato y eliminamos el salto de línea
puts "Hola mi nombre es " + b2 # mostramos el resultado

c2 = 'Mariana'
puts c2.class # con 'class' obtenemos el tipo de dato => String
c2 = 100
puts c2.class # con 'class' obtenemos el tipo de dato => Integer
c2 = 14.6
puts c2.class # con 'class' obtenemos el tipo de dato => Float

d2 = ("100").to_i # con 'to_i', convertimos un string numérico en Integer
puts d2.class
d2 = ("34.6").to_f # con 'to_f', convertimos un string numérico en FLoat
puts d2.class
d2 = (34).to_s # con 'to_s', convertimos un valor a String
puts d2.class

# ----------- Tipos de datos y sus métodos --------- #
# Para saber los métodos correspondientes a un tipo de dato, se debe utilizar 'methods'
# Ingresar en consola con 'irb'

9.methods # métodos para un dato de tipo Integer
"hola".methods # métodos para un dato de tipo String
