# -------------- EJERCICIO 1 ---------------- #
=begin

Crear un método que muestre en pantalla un saludo. El método debe recibir un parámetro, si ese parámetro es el string 'bye', el método debe mostrar en pantalla "byebye" 

=end

def saludo ( a = 'por defecto')
  puts 'un saludo'
  puts 'byebye' if a == 'bye'
end

saludo('hola')
saludo('bye')