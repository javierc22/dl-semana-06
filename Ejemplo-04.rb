# ------------- DIVISORES -------------- #

# Números divisibles por 10
(1..10).each do |i|
  puts i if (10 % i).zero?
end

# ------------- INTRODUCCION A MÉTODOS -------------- #
# Ejemplo 1:
def saludar
  puts 'Holaaa!'
end

saludar

# ------------ PASO DE PARÁMETROS en métodos -------------- #
# Ejemplo 1:
def saludar2(nombre)
  puts "Hola #{nombre}"
end

saludar2('Javier')
saludar2('Mariana')

# Ejemplo 2:
def sumar(a, b)
  puts a + b
end

sumar(4, 4)

# Ejemplo 3:
def incrementar(valor, incremento = 1) # incremento = 1 es el valor por defecto en caso de no recibir un parámetro
  puts valor + incremento
end

incrementar(2) # da como resultado 3
incrementar(5, 3) # da como resultado 8