# ------------------- INTRO A ARRAYS ---------------------#
a = [1, 5, '9', 10, 'Hola']
puts a.class # imprime el tipo de dato => Array
print a
puts
puts a[0] # imprime => 1
puts a[4] # imprime => 'Hola'
puts a[-1] # imprime => 'Hola'
puts a[-2] # imprime => 10

# insertar un valor al arreglo
a.push('guitarra')
print a
puts
# eliminar un elemento del arreglo
a.delete('guitarra')
print a