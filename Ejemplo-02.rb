# ----------- IF y UNLESS --------- #
# Forma normal
a = 2

if a == 2
  puts 'a vale 2'
end

b = 3

unless b == 5
  puts 'b es distinto de 5'
end

# Forma express
a = 2
puts "a vale 2" if a == 2

b = 3
puts "b es distinto de 5" unless a == 5

# ----------- El mayor de dos números --------- #
# EJERCICIO: El mayor de dos números
a = 7
b = 6

# 1era forma
puts 'A es mayor que B' if a > b
puts 'B es mayor que A' if b > a

# 2da forma
if a > b
  puts 'A es mayor que B'
elsif a == b
  puts 'A y B son iguales'
else
  puts 'B es mayor que A'
end

# ----------- Ciclo While --------- #
puts 'Ingrese el password...'
pass = gets.chomp

while pass != 'secreto'
  puts 'Ingrese el password nuevamente'
  pass = gets.chomp
end

puts 'password correto! :)'
