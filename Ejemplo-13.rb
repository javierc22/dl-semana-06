# ----------------- METODOS DE ARREGLOS --------------- #

# Ejercicio 1:
# Crear un método para obtener el promedio de un arreglo.
array_1 = [1,2,6,1,7,2,3]
array_2 = [5,5,5,5,5,5,5]

def promedio(array)
  suma = 0
  array.each do |element|
    suma += element
  end

  # calcular el promedio y transformar a 'float'
  suma / array.count.to_f
end

puts promedio(array_1)
puts promedio(array_2)

# Mejorando el código
def promedio2(array)
  suma = array.inject(&:+) / array.count.to_f # infect() suma todos los elementos del arreglo
end

puts promedio2(array_1)
puts promedio2(array_2)