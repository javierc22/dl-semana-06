# --------------- EL RETORNO  ---------------- #
def suma(a, b)
  a + b # también se puede escribir 'return a + b'
end

suma1 = suma(2,3)
puts suma1

# ------------- EJERCICIO: SUMA PARES --------------- #
# Cree un método que retorne una suma de los números pares entre 'a' y 'n', donde 'n' es pasado como parámetro.
def suma_pares(n)
  suma = 0
  (1..n).each do |i|
    suma += i if i.even? # even? = es par?
  end

  return suma
end

puts suma_pares(10)